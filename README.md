\documentclass{article}
\usepackage{graphicx} % Required for inserting images
\usepackage{tikz}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{tabularx}
\usepackage{multimedia}


\lstset{ 
    language=Python, % choose the language of the code
    basicstyle=\fontfamily{pcr}\selectfont\footnotesize\color[RGB]{150,150,102},
    keywordstyle=\color[RGB]{0,121,107}\bfseries, % style for keywords
    identifierstyle=\color[RGB]{120,120,150}, % style for identifiers (variable names)
    commentstyle=\color[RGB]{80,139,80}, % style for comments  
    numbers=none, % where to put the line-numbers 
    numberstyle=\tiny, % the size of the fonts that are used for the line-numbers     
    backgroundcolor=\color[RGB]{240, 240, 240},
    showspaces=false, % show spaces adding particular underscores
    showstringspaces=false, % underline spaces within strings
    showtabs=false, % show tabs within strings adding particular underscores
    frame=single, % adds a frame around the code
    tabsize=2, % sets default tabsize to 2 spaces
    rulesepcolor=\color{gray},
    rulecolor=\color{black},
    captionpos=b, % sets the caption-position to bottom
    breaklines=true, % sets automatic line breaking
    breakatwhitespace=false,
    escapechar=! % set the escape character
}



\title{Robotic Arm Position Simulation - A First Principles Analysis}
\author{Nathaniel Bechard, Ivan T. Ivanov}
\date{April 23 2023}

\begin{document}

\maketitle
\tableofcontents % Generates the contents page

\section{Abstract}
Robot arms are common in the industry

\newpage

\section{Introduction}
This paper demonstrates the derivation and programming of the equations of motion of a robotic arm from first principles.

\section{The 1-joint problem}
In this section, the equations of motion for a single motor, with a single joint will be derived. The Motor applies a torque $\tau_{app}(t)$ on a massless rod of length L. A point mass with a mass $m_1$ is connected to the end of the rod. 

\vspace{24pt}

\[\begin{tikzpicture}
  % Draw the circle
  \draw[->] (0, -0.2) arc (-90:270:0.2cm);
  \draw (0, 0.5) node {$\tau_{app}(t)$};
  \draw (0, -0.5) node {$\theta(t)$};
  
  % Draw the horizontal line
  \draw[thick] (0,0) -- (3,0);
  \draw (1.5,0) node[anchor=south] {$L$};
  
  % Draw the point mass
  \filldraw (3,0) circle (0.1cm);
  \draw (3,0) node[anchor=south west] {$m_1$};

  %draw gravity arrow
  \draw[->] (3,0) -- (3, -1);
  \draw (3,-0.5) node[anchor=east] {$m_1g$};

\end{tikzpicture}\]


\vspace{24pt}


\subsection{Deriving the equations of motion for the 1 joint problem}

\vspace{24pt}

\noindent The angular acceleration of the system can be determined with Newton's second law of rotational motion: $\sum_i \tau_i = I \alpha$. We can determine the net torque and rotational inertia for our particular system:

\vspace{24pt}

$\sum_i \tau_i = \tau_{app}(t) + \tau_{m_1g}(t)$

\vspace{24pt}

\noindent The torque applied by $m_1$ can be determined using the torque formula: $\tau_{m_1g}(t) = L*F*cos(\theta)$. The net torque is therefore:

\vspace{24pt}

$\sum_i \tau_i = \tau_{app}(t) - m_1gLcos(\theta(t))L$

\vspace{24pt}

\noindent The rotational inertia term, I, can be rewritten as $m_1*L^2$, and the angular acceleration can be turned into the second derivative of the angular position. After reorganizing terms, we get:
\vspace{24pt}

$\theta''(t) = \frac{\theta_{app}(t) - m_1gLcos(\theta(t))}{m_1L^2}$

\vspace{24pt}

\noindent Adding the friction term B changes the equation slightly:

\vspace{24pt}

$\theta''(t) + B\theta'(t) = \frac{\theta_{app}(t) - m_1gLcos(\theta(t))}{m_1L^2}$

\vspace{24pt}

\noindent It is helpful to disintegrate this equation into a system of first-order equations:

\vspace{24pt}
$\theta = X_1$    $\theta' = X_2$

\vspace{11pt}

$X_1' = X_2$   \hspace{1cm}  $X_2' = \frac{\theta_{app}(t) - m_1gLcos(X_1)}{m_1L^2} - BX_2$

\vspace{24pt}

\noindent We will use this system to solve the differential equation numerically

%FBD's, theory, results in a 2nd order, non-homogeneous, nonlinear Ordinary differential equation. 

\subsection{Programming the 1 joint problem}
While the code can be accessed on GitHub here:(Note to self: link later), an explanation of the important parts of it are in this document. 

\noindent To begin, simulation parameters are defined
\begin{lstlisting}[language=Python]

L = 1 #in meters, also known as L
B = 0.2 #rotational friction
Mass_1 = 1 # in kg, also known as M
motor_torque = 10 # in Nm, also known as Tq(t)
grav = 9.81 # m/s^2
simulationDuration = 20 # seconds

#initial conditions
initial_angle = 0 #rad
initial_angular_velocity = 0 #rad/s
initial_values = [initial_angle, initial_angular_velocity]

\end{lstlisting}

\newpage
\noindent Next, the differential equation is turned into a function which takes in the state of the system: $\vec{Y}$, and outputs its derivative, $\vec{Y}'$. The input vector has components $x_1$ and $x_2$
\vspace{24pt}

\begin{lstlisting}[language=Python]
def slopes(t, Y): 
    return([Y[1],        
          (motor_torque - Mass_1*grav*L*np.cos(Y[0])) / (Mass_1*L**2) - B*Y[1]
          ])
\end{lstlisting}

\vspace{24pt}
\noindent This function is passed through solve\_ivp, which solves the differential equation numerically. In this case, it uses a Runge Kutta 7-8 solver. The initial conditions are also plugged in, in vector form.
\vspace{24pt}

\begin{lstlisting}[language=Python]
sol = solve_ivp(slopes, [0, simulationDuration], initial_values, t_eval=T, method = 'DOP853', rtol=1e-8, atol=1e-8)

for x in range(len(sol.y[0])):
    print("position: " + str(sol.y[0, x]) + "velocity: " + str(sol.y[1, x]))
    
\end{lstlisting}

\vspace{24pt}
\noindent The solution is then animated. The angular coodi
\vspace{24pt}

\begin{lstlisting}[language=Python]
x1 = np.cos(sol.y[0]) * L #convert angular solution coordinates to cartesian for animating
y1 = np.sin(sol.y[0]) * L 

print(len(x1)) # prints how many frames the simulation has

def animate(i):
    x = [0, x1[i]]  
    y = [0, y1[i]]
    line.set_data(x, y) # make a line between (0,0) and (x1[i], y1[i])
    dots.set_data(x, y) # make dots at (0,0) and (x1[i], y1[i])
    txt.set_text('Time = ' +
    '{:4.1f}'.format(i*(simulationDuration/len(x1))) +
    's' + "  Velocity: " + '{:4.1f}'.format(sol.y[1,i])
    + " rad/s") # display time and velocity 
    return line, dots, txt

# call the animator.
anim = animation.FuncAnimation(fig, animate, init_func=init, 
frames=len(x1), interval=10, blit=True)  
# each frame iterates i by one. There is a 10ms delay between frames.
plt.show()
\end{lstlisting}
* cite project I took the time box from 

\subsection{Some Examples...}
\noindent Before moving on to multi-joint systems, let's investigate the properties of the one-joint system. The differential equation for this system provides valuable insight not visible with a formulaic understanding of mechanics. 






\newpage

\paragraph{No torque}
When the motor has no torque, the system behaves as a simple pendulum. It creates the characteristic whirlpool vector field. 
\begin{table}[h]
\centering
\begin{tabular}{|l|c|}
\hline
System property & \\
\hline
Length (L) & 1 m \\
Mass1 ($m_1$) & 1 kg \\
Torque & 0 Nm \\
Friction & 0.2 \\
\hline
\end{tabular}
\label{tab:quantities}
\end{table}
\begin{figure}[!h]
  \makebox[\textwidth][c]{\includegraphics[width=1.4\textwidth]{pendulum vector field.png}}%
  \caption{Vector field of a pendulum}
  \label{fig:key}
\end{figure}

\newpage

\paragraph{Damped-driven 1-joint problem}
Here, the mass is acted on by a torque of 50Nm. This is just enough to make it overcome the force of gravity: $9.81N * 5m = 49.05Nm$ when the bar is horizontal. The mass accelerates until it reaches $4\pm0.5\frac{rad}{s}$. This is the terminal velocity of the system, where the energy input by the motor is consumed entirely by friction. The steady-state velocity will vary between 4 and 5 radians per second, depending on whether it is fighting gravity or working with it. 
\begin{table}[h]
\centering
\begin{tabular}{|l|c|}
\hline
System property & \\
\hline
Length (L) & 5 m \\
Mass1 ($m_1$) & 1 kg \\
Torque & 50 Nm \\
Friction & 0.5 \\
\hline
\end{tabular}
\label{tab:quantities}
\end{table}
\begin{figure}[!h]
  \makebox[\textwidth][c]{\includegraphics[width=1.4\textwidth]{damped driven pendulum.png}}%
  \caption{Vector field of a driven 1-joint system}
  \label{fig:key}
\end{figure}

\newpage

\paragraph{Under-Powered Motor} 
When the motor doesn't have enough power to counter the force of gravity, it will not start spinning the mass. However, the wells of the vector field can be escaped with enough potential or kinetic energy. This will result in the mass rotating forever. Notice that the centers of the wells are at $-\pi/2 + 2\pi n$, with an angular velocity of zero. This is a state with no potential energy, where the arm is pointing downwards. When velocity is high enough above the well, or the position is far enough from downwards, the solution diverges. 
\begin{table}[h]
\centering
\begin{tabular}{|l|c|}
\hline
System property & \\
\hline
Length (L) & 1 m \\
Mass1 ($m_1$) & 1 kg \\
Torque & 7 Nm \\
Friction & 0.2 \\
\hline
\end{tabular}
\label{tab:quantities}
\end{table}
\begin{figure}[!h]
  \makebox[\textwidth][c]{\includegraphics[width=1.4\textwidth]{undertorqued problem.png}}%
  \caption{Vector field of a pendulum}
  \label{fig:key}
\end{figure}


\section{The robot arm}
\noindent This section covers the derivation simulation of the equations of motion for an arbitrary number of joints. 

The robot arm is a collection of 1-joint problems organized sequentially:

\vspace{24pt}

\begin{tikzpicture}


% Draw the lines
  \draw[thick] (0,0) -- (3,0);
  \draw (1.5,0) node[anchor=south] {$L_1$};
% Draw the dots
  \filldraw (3,0) circle (0.125cm);
  \draw (3,0.05) node[anchor=south east] {$m_1$};
%gravity vector
    \draw[->] (3,0) -- (3, -0.75);
% Draw the arcs and torques
  \draw[->] (0, -0.2) arc (-90:270:0.2cm);
  \draw (0, 0.5) node {$\tau_1$};
  \draw (0, -0.5) node {$\theta_1$};


   \draw[thick] (3,0) -- (5,1);
  \draw (4, 0.5) node[anchor=south] {$L_2$};
    \filldraw (5,1) circle (0.1cm);
  \draw (5.05,0.5) node[anchor=south west] {$m_2$};
    \draw[->] (5,1) -- (5, 0.5);
\draw[->] (3, -0.2) arc (-90:270:0.2cm);
  \draw (2.65, -0.25) node {$\tau_2$};
  \draw (3.35, -0.25) node {$\theta_2$};

  

\draw[thick] (5,1) -- (7,1);
\draw (6, 1) node[anchor=south] {$L_n$};
\filldraw (7,1) circle (0.15cm);
\draw (7,1) node[anchor=south west] {$m_n$};
\draw[->] (7,1) -- (7, 0);
\draw[->] (5, 0.8) arc (-90:270:0.2cm);
  \draw (4.65, 1.25) node {$\tau_n$};
  \draw (5.35, 1.25) node {$\theta_n$};


  %draw gravity arrow

\end{tikzpicture}

\vspace{14pt}
\noindent Note that $\tau_1 = \tau_1(t)$, time dependencies still apply, even if invisible
\vspace{24pt}

\subsection{Deriving the equations of motion for an arbitrary number of sequential 1-joint problems}

\paragraph{Rotational Inertia} Finding the rotational inertia and the torque exerted by gravity becomes complicated when more joints are involved. These quantities need to be computed with respect to each joint. For the first joint in the system:

\vspace{24pt}
$I_0 = m_nL_{0,mn}^2 + m_{n-1}L_{0,mn-1}^2 ... + m_1L_1^2$ , \\\\
where $I_0$ is the inertia calculated with respect to the first joint, and $L_{0,mn}$, is the distance from the first joint to the nth mass. $L_{0,mn-1}$ is the distance from the first joint to the second last mass.\\

These lengths are computed by treating the 1 joint problems downstream of the joint as  vectors of magnitude $L_i$ and direction $\theta_i$. These vectors can be summed to calculate the net vector. The vector below is computed from joint a (which we're calculating the inertia WRT to) to joint b, a joint we're trying to find the distance to. 

\vspace{24pt}
\centering $\vec{v}_{a,b} = J_a + J_{a+1} + J_b$,
\vspace{14pt}

\indent where $J_a =  \begin{bmatrix} L_acos(\theta_a) \\ L_asin(\theta_a) \end{bmatrix}$ 
 \hspace{1cm}   $|\vec{v}_{a,b}| = L_{a,b}$  \hspace{1cm} $\hat v_{a,b} = \theta_{a,b}$
 \vspace{24pt}

The rotational inertia with respect to a joint in the system can be computed from these quantities. This will be shown with code in the next section

\paragraph{torque applied by gravity} 
With the quantities just developed, the torque applied by gravity WRT to joint A can be computed.
\vspace{24pt}

$\tau_a = m_a*L_{a-1}*cos(\theta_a)*g + m_{a+1}*L_{a-1,a+1}*cos(\theta_{a-1,a+1})*g ... + m_{b}*L_{a-1,b}*cos(\theta_{a-1,b})*g$

\vspace{24pt}

*fix the notation mess here at some point

\subsection{Programming and simulating the robot arm}

\noindent The function passed through solve\_ivp is far more complex this time:
\vspace{24pt}
\begin{lstlisting}[language=Python]
def slopes(t, Input):     # Y is a vector of shape [x1, x2,..., xn]
    output = np.zeros(sn*2)

    #computes the distance between joints in an arm
    for joint_computed in range(sn):  #iterate joints
        rotational_inertia = 0 #rotational inertia WRT curr. joint
        net_torque_on_joint = 0 #net torque WRT curr joint
        for mass_computed in range((sn-1),(joint_computed-1),-1):
            lx, ly = 0, 0
            dist = 0
            #the vector sum
            for y in range(joint_computed, (mass_computed + 1)):
                lx = lx + Lengths[y]*np.cos(Input[y*2])
                ly = ly + Lengths[y]*np.sin(Input[y*2])
            dist = np.sqrt(lx**2 + ly**2) #The distance from Joint A to B   
            #The rotational inertia of the point mass computed is added to the system:
            rotational_inertia += Masses[mass_computed] * dist**2 
            net_torque_on_joint += Masses[mass_computed]*grav*dist*(lx/dist) 
            # lx/dist = adj/hyp = cos
        #take the inertia and torques calculated, and use them to compute the ODE:
        output[joint_computed*2] = Input[2*joint_computed + 1]
        output[joint_computed*2 +1] = ((Torques[joint_computed] - net_torque_on_joint) / rotational_inertia)  - Bs[joint_computed]*Input[2*joint_computed + 1] #x2'
    return(output)
    
\end{lstlisting}
Note: sn is the number of segments in the arm

\vspace{24pt}
The simulation parameters are now vectorized, and their vectors can be of an arbitrary length sn
\vspace{24pt}

\begin{lstlisting}[language=Python]
sn = 4

Lengths = [2, 1, 0.75, 2] # lenghths (m)
Bs = [0.5, 0.5, 0.5, 0.25] # friction terms 
Masses = [3, 5, 1, 3] # mass terms (kg)
Torques = [300, 3, 50, 5] # torque terms (Nm)
Init_values = [0.1,0.3,0.1,0.3,0.1,0.3,0,0] # position1, velocity1, position2, velocity2, ....., positionn, velocityn

grav = 9.81
simulationDuration = 20 # seconds
\end{lstlisting}

\vspace{24pt}

The animation code is very similar to the 1-joint problem. The full code can be found here *link git

\subsection{Simulations}
The n-joint arm exhibits complex behavior. It can be played with extensively using the source code here *link git

\section{Some experiments...}
- Torque as a function
- let the joints act as an arm
- Nick's DC motor integrated in the ODE

\section{Conclusion}

\end{document}
